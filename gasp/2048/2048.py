from gasp import games
from gasp import boards
from gasp import color
from gasp.utils import random_between

BOX_SIZE = 60
MARGIN = BOX_SIZE
EMPTY_COLOR = color.LIGHTGRAY
BOARD_COLOR = color.BEIGE


class TFEBox(boards.GameCell):
    def __init__(self, board, i, j):
        self.init_gamecell(board, i, j)
        self.number = None
        self.occupied = False

    def set_number(self, number):
        if type(number) is int:
            number = str(number)

        x, y = self.board.cell_to_coords(self.grid_x, self.grid_y)
        text_color = color.BLACK
        self.number = games.Text(self.screen, x + BOX_SIZE/2, y + BOX_SIZE/2, number,
                                 size=40, color=text_color)
        self.occupied = True

    def get_value(self):
        if self.number is not None:
            return self.number.get_text()

    def clear_cell(self):
        if self.number is not None:
            self.number.destroy()
        self.number = None
        self.occupied = False

    def move(self, direction):
        if self.number is None:
            return

        if self.direction[direction] is None:
            return

        value = self.get_value()
        self.clear_cell()
        self.direction[direction].set_number(value)
        return True


class TwentyFourtyEight(boards.SingleBoard):
    def __init__(self):
        self.init_singleboard((MARGIN, MARGIN), n_cols=4, n_rows=4, box_size=BOX_SIZE, fill_color=BOARD_COLOR)
        self.create_directions()
        self.set_background_color(EMPTY_COLOR)
        self.draw_all_outlines()

        self.new_block()
        # self.new_block()

    def new_gamecell(self, i, j):
        return TFEBox(self, i, j)

    def new_block(self):
        while True:
            i = random_between(0, 3)
            j = random_between(0, 3)
            if not self.grid[i][j].occupied:
                decision = random_between(0, 4)  # 5 choices
                if decision == 4:  # 20% chance
                    self.grid[i][j].set_number(4)
                else:
                    self.grid[i][j].set_number(2)
                break

    def collapse(self, direction):
        for row in self.grid:
            for box in row:
                box.move(direction)

    def keypress(self, key):
        if key == games.K_UP:
            direction = boards.UP
        elif key == games.K_DOWN:
            direction = boards.DOWN
        elif key == games.K_RIGHT:
            direction = boards.RIGHT
        elif key == games.K_LEFT:
            direction = boards.LEFT

        self.collapse(direction)


twentyfourtyeight = TwentyFourtyEight()
twentyfourtyeight.mainloop()
