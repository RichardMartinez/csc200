from gasp import games
from gasp import color
from gasp import cards
from gasp.utils import random_choice


class Blackjack(cards.SingleDeck):
    def __init__(self):
        self.init_singledeck(100, 100, size=100)
        self.set_background_color(color.GREEN)

    def tick(self):
        pass

    def hit(self):
        pos = (100, 100)
        new_card = self.get_hovered(pos)

        x, y = self.center_pos()
        y += 100
        new_card.move_to(x, y)

    def keypress(self, key):
        if key == games.K_SPACE:
            self.hit()


blackjack = Blackjack()
blackjack.mainloop()
