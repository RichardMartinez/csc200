from gasp import *

color_names = color.available(names_only=True)
index = 0

begin_graphics(width=600, height=600, title="Color Demonstration")

key = ""
while key != "Escape":
    if key == "space":
        index = (index + 1) % len(color_names)
    elif key == "BackSpace":
        index = (index - 1) % len(color_names)

    name = color_names[index]
    value = color.from_str(name)
    text_color = color.BLACK if name != "BLACK" else color.WHITE

    clear_screen(background=value)
    Text(f"{name}: {value}", (300, 300), color=text_color, size=20)

    controls = "Space to move forward; Backspace to move backward; Escape to quit"
    Text(controls, (300, 10), color=text_color, size=15)

    key = update_when("key_pressed")

end_graphics()
