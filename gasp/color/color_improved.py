#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# This program is part of GASP, a toolkit for newbie Python Programmers.
# Copyright 2020 (C) the GASP Development Team
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# Improved GASP Color System
# Developed by Richard Martinez

# This version is technically working like Jeff wanted, but I don't really like how complicated it had to
# become to accommodate. It honestly seems like overdoing it at this point.

# This version is designed to be used WITHOUT instantiating a color object ever; and while this version DOES
# do that, I think it just made more sense to have a pre-instantiated color object.

class Color:
    """
    Color container class that supports both RGB and HEX.
    """

    ALICEBLUE = "#F0F8FF"
    ANTIQUEWHITE = "#FAEBD7"
    AQUA = "#00FFFF"
    AZURE = "#F0FFD4"
    BEIGE = "#F5F5DC"
    BISQUE = "#F5F5DC"
    BLACK = "#000000"
    BLANCHEDALMOND = "#FFFFCD"
    BLUE = "#0000FF"
    BLUEVIOLET = "#8A2BE2"
    BROWN = "#A52A2A"
    BURLYWOOD = "#DEB887"
    CADETBLUE = "#5F9EA0"
    CHARTREUSE = "#7FFF00"
    CHOCOLATE = "#D2691E"
    CORAL = "#FF7F50"
    CORNFLOWERBLUE = "#6495ED"
    CORNSILK = "#FFF8DC"
    CRIMSON = "#DC143C"
    CYAN = "#00FFFF"
    DARKBLUE = "#00008B"
    DARKCYAN = "#008B8B"
    DARKGOLDENROD = "#B8860B"
    DARKGRAY = "#A9A9A9"
    DARKGREEN = "#006400"
    DARKKHAKI = "#BDB76B"
    DARKMAGENTA = "#8B008B"
    DARKOLIVEGREEN = "#556B2F"
    DARKORANGE = "#FF8C00"
    DARKORCHID = "#9B32CC"
    DARKRED = "#8B0000"
    DARKSALMON = "#E9967A"
    DARKSEAGREEN = "#8FBC8F"
    DARKSLATEBLUE = "#483D8B"
    DARKSLATEGRAY = "#2F4F4F"
    DARKTURQUOISE = "#00CED1"
    DARKVIOLET = "#9400D3"
    DEEPPINK = "#FF1493"
    DEEPSKYBLUE = "#00BFFF"
    DIMGRAY = "#696969"
    DODGERBLUE = "#1E90FF"
    FIREBRICK = "#B22222"
    FLORALWHITE = "#FFFAF0"
    FORESTGREEN = "#228B22"
    FUCHSIA = "#FF00FF"
    GAINSBORO = "#DCDCDC"
    GHOSTWHITE = "#F8F8FF"
    GOLD = "#FFD700"
    GOLDENROD = "#DAA520"
    GRAY = "#7F7F7F"
    GREEN = "#008000"
    GREENYELLOW = "#ADFF2F"
    HONEYDEW = "#F0FFF0"
    HOTPINK = "#FF69B4"
    INDIANRED = "#CD5C5C"
    INDIGO = "#4B0082"
    IVORY = "#FFF0F0"
    KHAKI = "#F0E68C"
    LAVENDER = "#E6E6FA"
    LAVENDERBLUSH = "#FFF0F5"
    LAWNGREEN = "#7CFCF5"
    LEMONCHIFFON = "#FFFACD"
    LIGHTBLUE = "#ADD8E6"
    LIGHTCORAL = "#F08080"
    LIGHTCYAN = "#E0FFFF"
    LIGHTGOLDENRODYELLOW = "#FAFAD2"
    LIGHTGREEN = "#90EE90"
    LIGHTGRAY = "#D3D3D3"
    LIGHTPINK = "#FFB6C1"
    LIGHTSALMON = "#FFA07A"
    LIGHTSEAGREEN = "#20B2AA"
    LIGHTSKYBLUE = "#87CEFA"
    LIGHTSLATEGRAY = "#778899"
    LIGHTSTEELBLUE = "#B0C4DE"
    LIGHTYELLOW = "#FFFFE0"
    LIME = "#00FF00"
    LIMEGREEN = "#32CD32"
    LINEN = "#FAF0E6"
    MAGENTA = "#FF00FF"
    MAROON = "#800000"
    MEDIUMAQUAMARINE = "#66CDAA"
    MEDIUMBLUE = "#0000CD"
    MEDIUMORCHID = "#BA55D3"
    MEDIUMPURPLE = "#9370DB"
    MEDIUMSEAGREEN = "#3CB371"
    MEDIUMSLATEBLUE = "#7B68EE"
    MEDIUMSPRINGGREEN = "#00FA9A"
    MEDIUMTURQOISE = "#48D1CC"
    MEDIUMVIOLETRED = "#C71585"
    MIDNIGHTBLUE = "#191970"
    MINTCREAM = "#F5FFFA"
    MISTYROSE = "#FFE4E1"
    MOCCASIN = "#FFE1B5"
    NAVAJOWHITE = "#FFDEAD"
    NAVY = "#000080"
    OLDLACE = "#FDF5E6"
    OLIVE = "#808000"
    OLIVEDRAB = "#6B8E23"
    ORANGE = "#FFA500"
    ORANGERED = "#FF4500"
    ORCHID = "#DA7AD6"
    PALEGOLDENROD = "#EEE8AA"
    PALEGREEN = "#98FB98"
    PALETURQOISE = "#AFEEEE"
    PALEVIOLETRED = "#DB7093"
    PAPAYAWHIP = "#FFEFD5"
    PEACHPUFF = "#FFEFD5"
    PERU = "#CD853F"
    PINK = "#FFC0CB"
    PLUM = "#D3A0DD"
    POWDERBLUE = "#B0E0E6"
    PURPLE = "#800080"
    RED = "#FF0000"
    ROSYBROWN = "#BC8F8F"
    ROYALBLUE = "#4169E1"
    SADDLEBROWN = "#8B4513"
    SALMON = "#FA8072"
    SANDYBROWN = "#F4A460"
    SEAGREEN = "#2E8B57"
    SEASHELL = "#FFF5EE"
    SIENNA = "#A0522D"
    SILVER = "#C0C0C0"
    SKYBLUE = "#87CEEB"
    SLATEBLUE = "#6A5ACD"
    SLATEGRAY = "#708090"
    SNOW = "#FFFAFA"
    SPRINGGREEN = "#00FF7F"
    STEELBLUE = "#4682B4"
    TAN = "#D2B48C"
    TEAL = "#008080"
    THISTLE = "#D8BFD8"
    TOMATO = "#FD6347"
    TURQUOISE = "#40E0D0"
    VIOLET = "#EE82EE"
    WHEAT = "#F5DEAA"
    WHITE = "#FFFFFF"
    WHITESMOKE = "#F5F5F5"
    YELLOW = "#FFFF00"
    YELLOWGREEN = "#9ACD32"

    mode = "HEX"

    # REMOVE ENTIRELY???
    # NOT WORKING CURRENTLY. NEVER GETTING RUN WHEN DOING Color.BLACK
    # @classmethod
    # def __getattr__(cls, item):
    #     """
    #     Returns stored color value in correct format.
    #     """
    #     try:
    #         if cls.mode == "HEX":
    #             return getattr(cls, str(item))
    #         elif cls.mode == "RGB":
    #             return cls.hex_to_rgb(getattr(cls, str(item)))
    #     except AttributeError:
    #         raise ValueError("Please enter a supported color.")

    # I had to make it so that set_mode re-sets every single color attribute to the new format.
    @classmethod
    def set_mode(cls, mode):
        """
        Sets mode between RGB and HEX. Pass argument as a string.
        """
        mode = mode.strip()
        pre_mode = cls.mode

        if mode in ["RGB", "HEX"]:
            cls.mode = mode
        else:
            raise ValueError("The two acceptable modes are 'RGB' and 'HEX'")

        post_mode = cls.mode

        if pre_mode == post_mode:  # To prevent errors
            return

        list_of_color_names = [string.split(":")[0] for string in Color.available()]

        if pre_mode == "HEX" and post_mode == "RGB":  # Moving from HEX to RGB
            for color_name in list_of_color_names:
                setattr(cls, color_name, cls.hex_to_rgb(getattr(cls, color_name)))
        elif pre_mode == "RGB" and post_mode == "HEX":  # Moving from RGB to HEX
            for color_name in list_of_color_names:
                setattr(cls, color_name, cls.rgb_to_hex(getattr(cls, color_name)))

    # I do like how simple available() has become however. Maybe we could use this somehow?
    @classmethod
    def available(cls):
        """
        Returns a list of all available colors.
        """
        lst = []

        for attr in dir(cls):
            if (not attr.startswith("__")) and attr == attr.upper():
                if cls.mode == "HEX":
                    lst.append(f"{attr}: {getattr(cls, attr)}")
                elif cls.mode == "RGB":
                    lst.append(f"{attr}: {cls.hex_to_rgb(getattr(cls, attr))}")

        return lst

    @staticmethod
    def hex_to_rgb(hex_string):
        """
        Converts a HEX string into an RGB three tuple. Leading # is optional.
        """
        try:
            if hex_string.startswith("#"):
                hex_string = hex_string[1:]
        except TypeError:
            raise TypeError("Input must be a string")

        return tuple(int(hex_string[i : i + 2], 16) for i in (0, 2, 4))

    @staticmethod
    def rgb_to_hex(*args):
        """
        Converts RGB values to a HEX string.
        Designed to be error proof: Can pass either as a three tuple, or exactly three passed parameters.
        """
        try:
            if len(args) == 1:
                args = tuple(n for n in args[0])
            return "#%02X%02X%02X" % args

        except Exception:
            raise TypeError(
                "Input must be a tuple. Either a single passed tuple, or exactly three passed params."
            )

    # Changed the name to accommodate always returning in the same order. I do like the new simplicity.
    @classmethod
    def get_hex_and_rgb(cls, color_name):
        color_name = color_name.strip()

        if color_name in dir(cls):
            hex_val = getattr(cls, color_name)
            rgb_val = cls.hex_to_rgb(hex_val)

            return color_name, hex_val, rgb_val
        else:
            raise ValueError(
                "The inputted color name is not part of the list of colors. "
                "Use color.available() to see the list"
            )


# Demo
# print(Color.BLUE)
# Color.set_mode("RGB")
# print(Color.BLUE)
