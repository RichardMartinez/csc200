from color_preferred import *


# A simplified version of normalize that works identically (tested).
def normalize(*args):
    rgb = 0

    if len(args) == 1:  # either hex, or single passed tuple, or color name
        arg = args[0]  # grabs the passed param

        if arg[0] == "#":  # looks like hex
            arg = arg[1:]
            for hue in range(0, 6, 2):
                rgb = (rgb << 8) + int(arg[hue:hue+2], 16)
        elif type(arg) is str:  # color name
            return "Color name handling should go here!"
        elif len(arg) == 3:  # single passed tuple
            for hue in arg:
                rgb = (rgb << 8) + hue
    elif len(args) == 3:  # three passed params (RGB)
        for hue in args:
            rgb = (rgb << 8) + hue

    return rgb


# Use the normalize() function as a throwaway to create these values.
class NormalValues:
    ALICEBLUE = 15792383               # (240, 248, 255) #F0F8FF
    ANTIQUEWHITE = 16444375            # (250, 235, 215) #FAEBD7
    AQUA = 65535                       # (0, 255, 255) #00FFFF
    AZURE = 15794132                   # (240, 255, 212) #F0FFD4
    BEIGE = 16119260                   # (245, 245, 220) #F5F5DC
    BISQUE = 16119260                  # (245, 245, 220) #F5F5DC
    BLACK = 0                          # (0, 0, 0) #000000
    BLANCHEDALMOND = 16777165          # (255, 255, 205) #FFFFCD
    BLUE = 255                         # (0, 0, 255) #0000FF
    BLUEVIOLET = 9055202               # (138, 43, 226) #8A2BE2
    BROWN = 10824234                   # (165, 42, 42) #A52A2A
    BURLYWOOD = 14596231               # (222, 184, 135) #DEB887
    CADETBLUE = 6266528                # (95, 158, 160) #5F9EA0
    CHARTREUSE = 8388352               # (127, 255, 0) #7FFF00
    CHOCOLATE = 13789470               # (210, 105, 30) #D2691E
    CORAL = 16744272                   # (255, 127, 80) #FF7F50
    CORNFLOWERBLUE = 6591981           # (100, 149, 237) #6495ED
    CORNSILK = 16775388                # (255, 248, 220) #FFF8DC
    CRIMSON = 14423100                 # (220, 20, 60) #DC143C
    CYAN = 65535                       # (0, 255, 255) #00FFFF
    DARKBLUE = 139                     # (0, 0, 139) #00008B
    DARKCYAN = 35723                   # (0, 139, 139) #008B8B
    DARKGOLDENROD = 12092939           # (184, 134, 11) #B8860B
    DARKGRAY = 11119017                # (169, 169, 169) #A9A9A9
    DARKGREEN = 25600                  # (0, 100, 0) #006400
    DARKKHAKI = 12433259               # (189, 183, 107) #BDB76B
    DARKMAGENTA = 9109643              # (139, 0, 139) #8B008B
    DARKOLIVEGREEN = 5597999           # (85, 107, 47) #556B2F
    DARKORANGE = 16747520              # (255, 140, 0) #FF8C00
    DARKORCHID = 10171084              # (155, 50, 204) #9B32CC
    DARKRED = 9109504                  # (139, 0, 0) #8B0000
    DARKSALMON = 15308410              # (233, 150, 122) #E9967A
    DARKSEAGREEN = 9419919             # (143, 188, 143) #8FBC8F
    DARKSLATEBLUE = 4734347            # (72, 61, 139) #483D8B
    DARKSLATEGRAY = 3100495            # (47, 79, 79) #2F4F4F
    DARKTURQUOISE = 52945              # (0, 206, 209) #00CED1
    DARKVIOLET = 9699539               # (148, 0, 211) #9400D3
    DEEPPINK = 16716947                # (255, 20, 147) #FF1493
    DEEPSKYBLUE = 49151                # (0, 191, 255) #00BFFF
    DIMGRAY = 6908265                  # (105, 105, 105) #696969
    DODGERBLUE = 2003199               # (30, 144, 255) #1E90FF
    FIREBRICK = 11674146               # (178, 34, 34) #B22222
    FLORALWHITE = 16775920             # (255, 250, 240) #FFFAF0
    FORESTGREEN = 2263842              # (34, 139, 34) #228B22
    FUCHSIA = 16711935                 # (255, 0, 255) #FF00FF
    GAINSBORO = 14474460               # (220, 220, 220) #DCDCDC
    GHOSTWHITE = 16316671              # (248, 248, 255) #F8F8FF
    GOLD = 16766720                    # (255, 215, 0) #FFD700
    GOLDENROD = 14329120               # (218, 165, 32) #DAA520
    GRAY = 8355711                     # (127, 127, 127) #7F7F7F
    GREEN = 32768                      # (0, 128, 0) #008000
    GREENYELLOW = 11403055             # (173, 255, 47) #ADFF2F
    HONEYDEW = 15794160                # (240, 255, 240) #F0FFF0
    HOTPINK = 16738740                 # (255, 105, 180) #FF69B4
    INDIANRED = 13458524               # (205, 92, 92) #CD5C5C
    INDIGO = 4915330                   # (75, 0, 130) #4B0082
    IVORY = 16773360                   # (255, 240, 240) #FFF0F0
    KHAKI = 15787660                   # (240, 230, 140) #F0E68C
    LAVENDER = 15132410                # (230, 230, 250) #E6E6FA
    LAVENDERBLUSH = 16773365           # (255, 240, 245) #FFF0F5
    LAWNGREEN = 8191221                # (124, 252, 245) #7CFCF5
    LEMONCHIFFON = 16775885            # (255, 250, 205) #FFFACD
    LIGHTBLUE = 11393254               # (173, 216, 230) #ADD8E6
    LIGHTCORAL = 15761536              # (240, 128, 128) #F08080
    LIGHTCYAN = 14745599               # (224, 255, 255) #E0FFFF
    LIGHTGOLDENRODYELLOW = 16448210    # (250, 250, 210) #FAFAD2
    LIGHTGRAY = 13882323               # (211, 211, 211) #D3D3D3
    LIGHTGREEN = 9498256               # (144, 238, 144) #90EE90
    LIGHTPINK = 16758465               # (255, 182, 193) #FFB6C1
    LIGHTSALMON = 16752762             # (255, 160, 122) #FFA07A
    LIGHTSEAGREEN = 2142890            # (32, 178, 170) #20B2AA
    LIGHTSKYBLUE = 8900346             # (135, 206, 250) #87CEFA
    LIGHTSLATEGRAY = 7833753           # (119, 136, 153) #778899
    LIGHTSTEELBLUE = 11584734          # (176, 196, 222) #B0C4DE
    LIGHTYELLOW = 16777184             # (255, 255, 224) #FFFFE0
    LIME = 65280                       # (0, 255, 0) #00FF00
    LIMEGREEN = 3329330                # (50, 205, 50) #32CD32
    LINEN = 16445670                   # (250, 240, 230) #FAF0E6
    MAGENTA = 16711935                 # (255, 0, 255) #FF00FF
    MAROON = 8388608                   # (128, 0, 0) #800000
    MEDIUMAQUAMARINE = 6737322         # (102, 205, 170) #66CDAA
    MEDIUMBLUE = 205                   # (0, 0, 205) #0000CD
    MEDIUMORCHID = 12211667            # (186, 85, 211) #BA55D3
    MEDIUMPURPLE = 9662683             # (147, 112, 219) #9370DB
    MEDIUMSEAGREEN = 3978097           # (60, 179, 113) #3CB371
    MEDIUMSLATEBLUE = 8087790          # (123, 104, 238) #7B68EE
    MEDIUMSPRINGGREEN = 64154          # (0, 250, 154) #00FA9A
    MEDIUMTURQOISE = 4772300           # (72, 209, 204) #48D1CC
    MEDIUMVIOLETRED = 13047173         # (199, 21, 133) #C71585
    MIDNIGHTBLUE = 1644912             # (25, 25, 112) #191970
    MINTCREAM = 16121850               # (245, 255, 250) #F5FFFA
    MISTYROSE = 16770273               # (255, 228, 225) #FFE4E1
    MOCCASIN = 16769461                # (255, 225, 181) #FFE1B5
    NAVAJOWHITE = 16768685             # (255, 222, 173) #FFDEAD
    NAVY = 128                         # (0, 0, 128) #000080
    OLDLACE = 16643558                 # (253, 245, 230) #FDF5E6
    OLIVE = 8421376                    # (128, 128, 0) #808000
    OLIVEDRAB = 7048739                # (107, 142, 35) #6B8E23
    ORANGE = 16753920                  # (255, 165, 0) #FFA500
    ORANGERED = 16729344               # (255, 69, 0) #FF4500
    ORCHID = 14318294                  # (218, 122, 214) #DA7AD6
    PALEGOLDENROD = 15657130           # (238, 232, 170) #EEE8AA
    PALEGREEN = 10025880               # (152, 251, 152) #98FB98
    PALETURQOISE = 11529966            # (175, 238, 238) #AFEEEE
    PALEVIOLETRED = 14381203           # (219, 112, 147) #DB7093
    PAPAYAWHIP = 16773077              # (255, 239, 213) #FFEFD5
    PEACHPUFF = 16773077               # (255, 239, 213) #FFEFD5
    PERU = 13468991                    # (205, 133, 63) #CD853F
    PINK = 16761035                    # (255, 192, 203) #FFC0CB
    PLUM = 13869277                    # (211, 160, 221) #D3A0DD
    POWDERBLUE = 11591910              # (176, 224, 230) #B0E0E6
    PURPLE = 8388736                   # (128, 0, 128) #800080
    RED = 16711680                     # (255, 0, 0) #FF0000
    ROSYBROWN = 12357519               # (188, 143, 143) #BC8F8F
    ROYALBLUE = 4286945                # (65, 105, 225) #4169E1
    SADDLEBROWN = 9127187              # (139, 69, 19) #8B4513
    SALMON = 16416882                  # (250, 128, 114) #FA8072
    SANDYBROWN = 16032864              # (244, 164, 96) #F4A460
    SEAGREEN = 3050327                 # (46, 139, 87) #2E8B57
    SEASHELL = 16774638                # (255, 245, 238) #FFF5EE
    SIENNA = 10506797                  # (160, 82, 45) #A0522D
    SILVER = 12632256                  # (192, 192, 192) #C0C0C0
    SKYBLUE = 8900331                  # (135, 206, 235) #87CEEB
    SLATEBLUE = 6970061                # (106, 90, 205) #6A5ACD
    SLATEGRAY = 7372944                # (112, 128, 144) #708090
    SNOW = 16775930                    # (255, 250, 250) #FFFAFA
    SPRINGGREEN = 65407                # (0, 255, 127) #00FF7F
    STEELBLUE = 4620980                # (70, 130, 180) #4682B4
    TAN = 13808780                     # (210, 180, 140) #D2B48C
    TEAL = 32896                       # (0, 128, 128) #008080
    THISTLE = 14204888                 # (216, 191, 216) #D8BFD8
    TOMATO = 16606023                  # (253, 99, 71) #FD6347
    TURQUOISE = 4251856                # (64, 224, 208) #40E0D0
    VIOLET = 15631086                  # (238, 130, 238) #EE82EE
    WHEAT = 16113322                   # (245, 222, 170) #F5DEAA
    WHITE = 16777215                   # (255, 255, 255) #FFFFFF
    WHITESMOKE = 16119285              # (245, 245, 245) #F5F5F5
    YELLOW = 16776960                  # (255, 255, 0) #FFFF00
    YELLOWGREEN = 10145074             # (154, 205, 50) #9ACD32


# Once colors are stored as 24-bit integers, use this function to get back either a HEX string or RGB tuple.
def return_color(normal, mode):
    """
    Translate a given normalized integer into either HEX or RGB.

    Normal is a normalized 24 bit int.
    Mode is a string "HEX" or "RGB"
    """
    if mode == "HEX":
        return f"#{normal:06X}"
    elif mode == "RGB":
        return normal >> 16 & 0xFF, normal >> 8 & 0xFF, normal & 0xFF


def main():
    # Demo to show that Normalized System returns the same values as before
    for color_name in color.available(names_only=True):

        #### Existing System ####
        _, hex_val, rgb_val = color.get_hex_and_rgb(color_name)

        print("Existing System:")
        print(f"{color_name} -> {hex_val}, {rgb_val}")

        #### Normalized System ####
        normalized = getattr(NormalValues, color_name)
        hex_val = return_color(normalized, "HEX")
        rgb_val = return_color(normalized, "RGB")

        print("Normalized System:")
        print(f"{color_name} -> {hex_val}, {rgb_val}")

        # Dashes for readability in terminal
        print("-"*30)


# Just used to format comments in NormalValues
# def throwaway():
#     with open("test.txt", "w") as f:
#         f.write('"""' + "\n")
#         for color_name in color.available(names_only=True):
#             normalized = getattr(NormalValues, color_name)
#             hex_str = str(return_color(normalized, "HEX"))
#             rgb_str = str(return_color(normalized, "RGB"))
#
#             lhs = f'"{color_name}": {normalized},'
#             mhs = f"#    {rgb_str}"
#             rhs = str(hex_str)
#
#             line = "{:<35s}".format(lhs) + "{:<25s}".format(mhs) + rhs + "\n"
#             f.write(line)
#         f.write('"""')


if __name__ == '__main__':
    main()
