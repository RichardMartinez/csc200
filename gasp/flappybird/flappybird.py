from gasp import games
from gasp import color
from gasp.utils import random_between


# This function could go inside gasp.utils. It is not only useful here, but would be useful
# in a lot of other games.
def map_ranges(range1, range2, n, in_bounds_only=True):
    """
    Maps |range1| to |range2|.
    For example, if |n| is halfway through |range1|, return halfway though |range2|.
    """
    a, b = range1
    c, d = range2

    distance1 = b - a
    distance2 = d - c

    percentage = (n - a) / distance1

    if in_bounds_only:
        if percentage < 0:
            percentage = 0
        elif percentage > 1:
            percentage = 1

    return c + percentage * distance2


class Bird(games.Animation):
    def __init__(self, screen, x, y):
        repeating = ["assets/bird-downflap.png",
                     "assets/bird-midflap.png",
                     "assets/bird-upflap.png"]
        nonrepeating, repeating = games.load_animation(nonrepeating_files=[], repeating_files=repeating)
        self.init_animation(screen, x, y, nonrepeating, repeating, repeat_interval=10)
        self.velocity = (0, 0)

    def apply_gravity(self):
        vx, vy = self.velocity
        vy += 0.1
        self.velocity = vx, vy

    def flap(self):
        # Remember negative y velocity means UP
        self.velocity = (0, -5)
        self.screen.play_sound("flap")

    def fall_down(self):
        self.apply_gravity()
        self.move_by(self.velocity)

    def look(self):
        velocity_range = (-5, 5)
        angle_range = (-25, 25)

        vx, vy = self.velocity
        angle = map_ranges(velocity_range, angle_range, vy)

        # negative velocity means going UP, so angle must be positive
        self.rotate_to(-angle)


class Pipe(games.Sprite, games.Mover):
    def __init__(self, screen, x, y, facing_down=False):
        self.image = games.load_image("assets/pipe.png", transparent=0)

        # Make the image 350 pixels tall
        self.image = games.set_resolution(self.image, y_resolution=350)

        self.width = self.image.get_width()
        self.height = self.image.get_height()

        # x and y should correspond to the top left, but Sprite wants the center
        x += self.width / 2
        y += self.height / 2

        if facing_down:
            self.image = games.flip_image(self.image, vertical=True)

        self.init_sprite(screen,
                         x,
                         y,
                         image=self.image)
        self.init_mover(-2, 0)  # Moving to the left


class PipePair:
    def __init__(self, screen, start_x):
        self.screen = screen
        self.start_x = start_x
        self.start_y = random_between(-300, 0)  # Choose a height that still guarantees pipes on screen

        self.top_pipe = Pipe(self.screen, self.start_x, self.start_y, facing_down=True)

        PIPE_HEIGHT = self.top_pipe.height
        PIPE_GAP = 200
        self.bottom_pipe = Pipe(self.screen, self.start_x,
                                self.start_y + PIPE_HEIGHT + PIPE_GAP,
                                facing_down=False)

    def on_screen(self):
        return self.top_pipe.xpos() + self.top_pipe.width > 0  # Right side is still on screen

    def destroy(self):
        self.top_pipe.destroy()
        self.bottom_pipe.destroy()

    def hit(self):
        # If either of the pipes overlap, return True
        if self.top_pipe.overlaps(self.screen.bird) or self.bottom_pipe.overlaps(self.screen.bird):
            return True
        return False

    def stop(self):
        self.top_pipe.set_velocity(0, 0)
        self.bottom_pipe.set_velocity(0, 0)


class FlappyBird(games.Screen):
    def __init__(self, width, height):
        self.init_screen(width=width, height=height, title="Flappy Bird")
        BACKGROUND_IMG = games.load_image("assets/background.png", transparent=0)

        # Scale the background image to be as tall as the screen
        BACKGROUND_IMG = games.set_resolution(BACKGROUND_IMG, y_resolution=self.get_height())

        self.set_background(BACKGROUND_IMG)

        GROUND_IMG = games.load_image("assets/ground.png", transparent=0)
        GROUND_IMG = games.set_resolution(GROUND_IMG, x_resolution=self.get_width())

        # x and y should correspond to the top left, but Sprite wants the center
        x = 0
        y = self.get_height() - GROUND_IMG.get_height() / 3
        self.ground = games.Sprite(self,
                                   x + GROUND_IMG.get_width()/2,
                                   y + GROUND_IMG.get_height()/2,
                                   image=GROUND_IMG)

        self.game_over = 0

        # Init game sounds
        self.GAME_SOUNDS = {
            "flap": games.load_sound("assets/flap.wav"),
            "point": games.load_sound("assets/point.wav"),
            "hit": games.load_sound("assets/hit.wav")
        }

        # Init Bird
        self.bird = Bird(self, self.get_width()/6, self.get_height()/2)

        # Init pipe pairs
        pipe_pair1 = PipePair(self, self.get_width())

        PIPE_WIDTH = pipe_pair1.top_pipe.width
        pipe_pair2 = PipePair(self, self.get_width() * 1.5 + PIPE_WIDTH)

        # Install to pipe_pairs list
        self.pipe_pairs = [
            pipe_pair1,
            pipe_pair2
        ]

        # Init point counter
        self.counter = games.Text(self,
                                  x=self.get_width()/2,
                                  y=self.get_height()/8,
                                  text="0",
                                  size=50,
                                  color=color.WHITE)

    def tick(self):
        if self.game_over:
            return

        self.bird.fall_down()
        self.bird.look()

        # First pipe pair is the only one that can move off screen
        # It is the only one that can get hit too
        # Order is maintained in new_pipe_pair
        first_pipe = self.pipe_pairs[0]
        if not first_pipe.on_screen():
            first_pipe.destroy()
            self.new_pipe_pair()

        # End the game if:
        # Hit a pipe
        # Hit the ground
        # Or go too far above the screen (prevent cheating) (give a little bit of leeway)
        if first_pipe.hit() or self.ground.overlaps(self.bird) or self.bird.ypos() < -20:
            self.end_game()

        # Ensure counter is always seen
        self.counter.raise_object()

    def keypress(self, key):
        if self.game_over:
            return

        if key == games.K_SPACE:
            self.bird.flap()

    def new_pipe_pair(self):
        self.add_point()
        pipe_pair = PipePair(self, self.get_width())

        # Redeclare pipe pairs in right order
        self.pipe_pairs = [self.pipe_pairs[1], pipe_pair]

    def play_sound(self, sound_name):
        self.GAME_SOUNDS[sound_name].play()

    def add_point(self):
        self.play_sound("point")
        score = int(self.counter.get_text())
        score += 1
        self.counter.set_text(str(score))

    def end_game(self):
        self.play_sound("hit")
        self.game_over = 1
        for pipe_pair in self.pipe_pairs:
            pipe_pair.stop()
        self.bird.velocity = (0, 0)
        self.bird.stop()  # Ends the flapping animation

        # Show Game Over Text
        self.counter.move_to(self.get_width()/2, self.get_height()/2)
        self.counter.set_color(color.BLUE)
        final_score = self.counter.get_text()
        self.counter.set_text(f"Game Over!  Final Score: {final_score}")


flappybird = FlappyBird(600, 600)
flappybird.mainloop()
