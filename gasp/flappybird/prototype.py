from gasp import games
from gasp import color
from gasp.utils import random_between


# This function could go inside gasp.utils. It is not only useful here, but would be useful
# in a lot of other games.
def map_ranges(range1, range2, n, in_bounds_only=True):
    """
    Maps |range1| to |range2|.
    For example, if |n| is halfway through |range1|, return halfway though |range2|.
    """
    a, b = range1
    c, d = range2

    distance1 = b - a
    distance2 = d - c

    percentage = (n - a) / distance1

    if in_bounds_only:
        if percentage < 0:
            percentage = 0
        elif percentage > 1:
            percentage = 1

    return c + percentage * distance2


# This could also go in utils?
def box_shape(width, height):
    """
    Returns a box shape with width and height; suitable for use in the Polygon class.
    """
    return [(0, 0), (width, 0), (width, height), (0, height)]


BIRD_WIDTH, BIRD_HEIGHT = 30, 20
BIRD_SHAPE = box_shape(BIRD_WIDTH, BIRD_HEIGHT)

PIPE_WIDTH, PIPE_HEIGHT = 50, 350
PIPE_SHAPE = box_shape(PIPE_WIDTH, PIPE_HEIGHT)
PIPE_GAP = 200


class Bird(games.Polygon, games.Mover):
    def __init__(self, screen, x, y):
        self.init_polygon(screen, x, y, BIRD_SHAPE, color=color.YELLOW)
        self.init_mover(0, 0)

    def apply_gravity(self):
        vx, vy = self.get_velocity()
        vy += 0.1
        self.set_velocity(vx, vy)

    def flap(self):
        self.set_velocity(0, -5)

    def look(self):
        velocity_range = (-5, 5)
        angle_range = (-45, 45)

        vx, vy = self.get_velocity()
        angle = map_ranges(velocity_range, angle_range, vy)

        self.rotate_to(-angle)


class Pipe(games.Polygon, games.Mover):
    def __init__(self, screen, x, y):
        self.init_polygon(screen, x, y, PIPE_SHAPE, color=color.GREEN)
        self.init_mover(-2, 0)


class PipePair:
    def __init__(self, screen, start_x):
        self.screen = screen
        self.start_x = start_x
        self.start_y = random_between(-300, 0)

        self.top_pipe = Pipe(self.screen, self.start_x, self.start_y)
        self.bottom_pipe = Pipe(self.screen, self.start_x, self.start_y + PIPE_HEIGHT + PIPE_GAP)

    def on_screen(self):
        return self.top_pipe.xpos() + PIPE_WIDTH > 0  # Right side is still on screen

    def destroy(self):
        self.top_pipe.destroy()
        self.bottom_pipe.destroy()

    def stop(self):
        self.top_pipe.set_velocity(0, 0)
        self.bottom_pipe.set_velocity(0, 0)

    def hit(self):
        if self.top_pipe.overlaps(self.screen.bird) or self.bottom_pipe.overlaps(self.screen.bird):
            return True
        return False


class FlappyBird(games.Screen):
    def __init__(self, width, height):
        self.init_screen(width=width, height=height, title="Flappy Bird PROTOTYPE")
        self.set_background_color(color.SKYBLUE)
        self.game_over = 0

        self.bird = Bird(self, self.get_width()/6, self.get_height()/2)
        self.pipe_pair1 = PipePair(self, self.get_width())
        self.pipe_pair2 = PipePair(self, self.get_width() * 1.5 + PIPE_WIDTH)  # Current width, plus half the screen
        self.counter = games.Text(self, self.get_width()/2, self.get_height()/8, "0", size=50, color=color.WHITE)

        GROUND_HEIGHT = 25
        GROUND_SHAPE = box_shape(self.get_width(), self.get_height())
        self.ground = games.Polygon(self, 0, self.get_height() - GROUND_HEIGHT, GROUND_SHAPE, color.LIGHTGREEN)

    def tick(self):
        if self.game_over:
            return

        self.bird.look()
        self.bird.apply_gravity()

        if not self.pipe_pair1.on_screen():
            self.pipe_pair1.destroy()
            self.new_pipe_pair(1)

        if not self.pipe_pair2.on_screen():
            self.pipe_pair2.destroy()
            self.new_pipe_pair(2)

        if self.pipe_pair1.hit() or self.pipe_pair2.hit() or self.ground.overlaps(self.bird):
            self.end_game()

        self.counter.raise_object()
        self.ground.raise_object()

    def keypress(self, key):
        if self.game_over:
            return

        if key == games.K_SPACE:
            self.bird.flap()

    def new_pipe_pair(self, number):
        self.add_point()
        if number == 1:
            self.pipe_pair1 = PipePair(self, self.get_width())
        elif number == 2:
            self.pipe_pair2 = PipePair(self, self.get_width())

    def add_point(self):
        score = int(self.counter.get_text())
        score += 1
        self.counter.set_text(str(score))

    def end_game(self):
        self.game_over = 1
        self.pipe_pair1.stop()
        self.pipe_pair2.stop()
        self.bird.set_velocity(0, 0)
        final_score = self.counter.get_text()
        self.counter.move_to(self.get_width()/2, self.get_height()/2)
        self.counter.set_color(color.BLUE)
        self.counter.set_text(f"You Lose!  Final Score: {final_score}")


flappybird = FlappyBird(600, 600)
flappybird.mainloop()
