from random import randint
from gasp.utils import read_number  # read_yesorno is broken at the moment see issue


def read_yesorno(prompt="Yes or no? "):
    whitespace = " \t\n\r\x0b\x0c"
    if prompt != "" and prompt[-1] not in whitespace:
        prompt += " "

    while True:
        result = input(prompt)
        if result.lower() in ["y", "yes"]:
            return True
        elif result.lower() in ["n", "no"]:
            return False
        print("Please answer yes or no.\n")


def play():
    answer = randint(1, 1000)
    print("OK, I've thought of a number between 1 and 1000.\n")

    num_guesses = 0
    while True:
        guess = read_number("Make a guess. ")
        num_guesses += 1

        if guess == answer:
            print("That was my number. Well done!\n")
            break

        if guess > answer:
            print("That's too high.\n")
        else:
            print("That's too low.\n")

    print(f"You took {num_guesses} guesses.")
    replay = read_yesorno("Would you like another game? ")

    if replay:
        print("-"*20)
        play()
    else:
        print("OK. Bye!")


play()
