import random

num_questions = int(input("How many questions should there be? "))
operators = ["+", "-", "*"]
score = 0
for i in range(num_questions):
    num1, num2 = random.randint(1, 10), random.randint(1, 10)
    operator = random.choice(operators)
    if operator == "+":
        answer = num1 + num2
    elif operator == "-":
        answer = num1 - num2
    elif operator == "*":
        answer = num1 * num2
    else:
        answer = 0

    if int(input(f"What's {num1} {operator} {num2}? ")) == answer:
        print("That's right - well done.")
        score += 1
    else:
        print(f"No, I’m afraid the answer is {answer}.")

print(f"\nI asked you {num_questions} questions. You got {score} of them right.\nWell done!")