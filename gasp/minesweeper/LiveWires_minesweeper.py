from gasp import games
from gasp import boards
from gasp import color
from gasp.utils import random_between

###################################################
# THIS VERSION IS BROKEN                          #
# SEE NEW VERSION OF minesweeper.py               #
###################################################

BOX_SIZE = 40
MARGIN = 60

MINE_SIZE = BOX_SIZE / 2 - 3

NUMBER_SIZE = 24
TEXT_SIZE = 18

FLAG = ((BOX_SIZE/4, 2), (BOX_SIZE/2, BOX_SIZE-2), (3*BOX_SIZE/4, 2))


class GridBox(boards.GameCell):
    def __init__(self, screen, i, j):
        self.init_gamecell(screen, i, j)
        self.flagged = 0
        self.shown = 0
        self.image = None
        self.flag = None


class Mine(boards.Container, GridBox):
    def __init__(self, board, i, j):
        self.init_container(['image', 'flag'])
        self.init_gamecell(board, i, j)

        x_pos, y_pos = board.cell_to_coords(i, j)
        self.image = games.Circle(self.screen,
                                  x_pos + BOX_SIZE / 2,
                                  y_pos + BOX_SIZE / 2,
                                  MINE_SIZE,
                                  color.BLACK,
                                  filled=True)

        self.lower_object()

    def reveal_mine(self):
        self.set_color(color.RED)
        # self.image = games.Circle(self.screen,
        #                           self.xpos() + BOX_SIZE/2,
        #                           self.ypos() + BOX_SIZE/2,
        #                           MINE_SIZE,
        #                           color.BLACK,
        #                           filled=True)
        self.raise_object()
        self.flagged = 0
        if self.flagged:
            self.flag.raise_object()

    def reveal_box(self):
        return not self.flagged

    def is_mine(self):
        return True


class EmptyBox(Mine):
    def reveal_mine(self):
        pass

    def is_mine(self):
        return False

    def reveal_box(self):
        if self.shown or self.flagged:
            return False

        count = 0
        for n in self.neighbors:
            if n.is_mine():
                count += 1

        self.set_color(color.WHITE)
        self.image = games.Text(self.screen,
                                self.xpos() + BOX_SIZE/2,
                                self.ypos() + BOX_SIZE/2,
                                str(count),
                                NUMBER_SIZE,
                                color.BLUE)
        self.shown = 1
        self.board.revealed_box()
        if count == 0:
            for n in self.neighbors:
                n.reveal_box()
        return False


class Minesweeper(boards.SingleBoard):
    def __init__(self, n_cols, n_rows, mines_to_find):
        self.mines_to_find = mines_to_find
        self.flag_count = 0
        self.unknown_count = n_cols * n_rows
        self.game_over = 0

        self.init_singleboard((MARGIN, MARGIN), n_cols, n_rows, BOX_SIZE)
        self.set_background_color(color.LIGHTGRAY)

        for m in range(mines_to_find):
            while True:
                i = random_between(0, n_cols - 1)
                j = random_between(0, n_rows - 1)
                if self.grid[i][j] is None:
                    break
            self.grid[i][j] = Mine(self, i, j)

        for i in range(n_cols):
            for j in range(n_rows):
                if self.grid[i][j] is None:
                    self.grid[i][j] = EmptyBox(self, i, j)

        self.create_neighbors()
        self.enable_cursor(0, 0)

        self.draw_all_outlines()

    def new_gamecell(self, i, j):
        return None

    def end_game(self):
        self.game_over = 1
        self.map_grid(reveal_all_mines)
        self.disable_cursor()

    def keypress(self, key):
        self.handle_cursor(key)

        if self.game_over:
            return

        if key == games.K_RETURN:
            if self.cursor.reveal_box():
                self.end_game()

    def revealed_box(self):
        self.unknown_count -= 1


def reveal_all_mines(cell):
    cell.reveal_mine()


minefield = Minesweeper(8, 8, 10)
minefield.mainloop()
