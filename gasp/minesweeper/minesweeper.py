from gasp import games
from gasp import boards
from gasp import color
from gasp.utils import random_between

BOX_SIZE = 40
MARGIN = 60

MINE_SIZE = BOX_SIZE / 2 - 3

NUMBER_SIZE = 24
NUMBER_COLORS = {
    1: color.BLUE,
    2: color.GREEN,
    3: color.RED,
    4: color.PURPLE,
    5: color.MAROON,
    6: color.TURQUOISE,
    7: color.BLACK,
    8: color.GRAY
}

TEXT_SIZE = 18

FLAG_SHAPE = ((BOX_SIZE/4, 2), (BOX_SIZE/2, BOX_SIZE-2), (3*BOX_SIZE/4, 2))


class GridBox(boards.GameCell):
    def __init__(self, board, i, j):
        self.init_gamecell(board, i, j)
        self.shown = False

    def reveal_box(self):
        # Prevent Accidental Key Presses
        if self.shown or self.is_flagged():
            return False

        self.board.revealed_box()

        # Determine the Number of Mines Near Me
        self.count = 0
        for n in self.neighbors:
            if n.is_mine():
                self.count += 1

        self.set_color(color.WHITE)
        self.shown = True

        # Generate My Number on Screen (0 does not show up)
        if self.count > 0:
            self.image = games.Text(self.screen,
                                    self.xpos() + BOX_SIZE/2,
                                    self.ypos() + BOX_SIZE/2,
                                    str(self.count),
                                    size=NUMBER_SIZE,
                                    color=NUMBER_COLORS[self.count])

        # Auto Clear Empty Space
        if self.count == 0:
            for n in self.neighbors:
                n.reveal_box()
        return False

    def is_flagged(self):
        return self in self.board.flags

    def is_mine(self):
        return self in self.board.mines

    def toggle_flag(self):
        if not self.is_flagged():
            self.flag = games.Polygon(self.screen, self.xpos(), self.ypos(), FLAG_SHAPE, color.RED)
            self.board.flags.append(self)
        else:
            self.flag.destroy()
            self.board.flags.remove(self)


class Mine(GridBox):
    def __init__(self, board, i, j):
        self.init_gamecell(board, i, j)
        self.shown = False

    def reveal_box(self):
        # Prevent Accidental Key Presses / Reveal Mines if Game is Over
        if self.is_flagged() and not self.board.game_over:
            return False

        self.set_color(color.RED)
        self.shown = True

        # Draw Mine Circle on Screen
        x, y = self.board.cell_to_coords(self.grid_x, self.grid_y)
        self.image = games.Circle(self.board,
                                  x + BOX_SIZE / 2,
                                  y + BOX_SIZE / 2,
                                  MINE_SIZE,
                                  color=color.BLACK)
        return True  # The Game is Over


class Minesweeper(boards.SingleBoard):
    def __init__(self, n_cols, n_rows, mines_to_find):
        self.mines_to_find = mines_to_find
        self.unknown_count = n_cols * n_rows
        self.game_over = 0

        self.init_singleboard((MARGIN, MARGIN), n_cols, n_rows, BOX_SIZE)
        self.set_background_color(color.LIGHTGRAY)

        # Create Mines
        self.mines = []
        for m in range(mines_to_find):
            while True:
                i = random_between(0, n_cols - 1)
                j = random_between(0, n_rows - 1)
                if not self.grid[i][j].is_mine():  # Not Occupied
                    break
            self.grid[i][j] = Mine(self, i, j)
            self.mines.append(self.grid[i][j])

        # Keep Track of Flags
        self.flags = []

        self.create_neighbors()
        self.enable_cursor(0, 0)

        x = self.screen.get_width() / 2  # Center of the Screen Horizontally
        y = MARGIN / 2  # Halfway down the MARGIN
        self.text = games.Text(self.screen, x, y, "M  I  N  E  S  W  E  E  P  E  R", size=40, color=color.WHITE)

    def new_gamecell(self, i, j):
        return GridBox(self, i, j)

    def keypress(self, key):
        self.handle_cursor(key)

        if self.game_over:
            return

        # Reveal this box
        if key == games.K_RETURN:
            if self.cursor.reveal_box():  # Returns True if box is a mine
                self.lose_game()

        # Flag this box
        if key == games.K_SPACE:
            self.cursor.toggle_flag()

    def tick(self):
        # Did we win?
        if self.detect_win():
            self.win_game()

        # Make sure all outlines are drawn
        self.draw_all_outlines()

    def detect_win(self):
        # In order to win:
        # All mines must be flagged
        # and all spaces (other than mines) must be uncovered

        for mine in self.mines:
            if not mine.is_flagged():
                return False  # If any of the flags are not flagged, we didn't win yet

        if self.mines_to_find != self.unknown_count:  # If not all spaces have been uncovered
            return False

        return True  # Otherwise, we win!

    def reveal_all_mines(self):
        for mine in self.mines:
            if not mine.shown:
                mine.reveal_box()

        # Toggle off all the flags
        for flag in reversed(self.flags):
            flag.toggle_flag()

    def revealed_box(self):
        self.unknown_count -= 1

    def lose_game(self):
        self.game_over = 1
        self.reveal_all_mines()
        self.disable_cursor()
        self.text.set_text("You Lose!")
        self.text.set_color(color.RED)

    def win_game(self):
        self.game_over = 1
        self.reveal_all_mines()
        self.disable_cursor()
        self.text.set_text("You Win!")
        self.text.set_color(color.GREEN)


# Easy: (8 rows, 8 cols, 10 mines)
# Medium: (16 rows, 16 cols, 40 mines)
# Hard: (20 rows, 20 cols, 70 mines)
minesweeper = Minesweeper(8, 8, 10)
minesweeper.mainloop()
