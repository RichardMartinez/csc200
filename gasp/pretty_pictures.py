from gasp import *


def modify_pos(pos, dx, dy):
    return pos[0] + dx, pos[1] + dy


def draw_face(x, y):
    """
    Draws a face at the given center position.
    """
    center_pos = (x, y)
    Circle(center_pos, 40)  # Head
    Circle(modify_pos(center_pos, -15, 10), 5)  # Left Eye
    Circle(modify_pos(center_pos, 15, 10), 5)  # Right Eye
    Line(modify_pos(center_pos, 0, 10), modify_pos(center_pos, -10, -10), thickness=1)  # Nose Line 1
    Line(modify_pos(center_pos, -10, -10), modify_pos(center_pos, 10, -10), thickness=1)  # Nose Line 2
    Arc(center_pos, 30, 225, 90)  # Smile


def draw_face2(x, y, scalar=1.0):
    """
    Draws a face at the given center position with an optional scalar.
    """
    center_pos = (x, y)
    radius = 40 * scalar
    Circle(center_pos, radius)  # Head
    Circle(modify_pos(center_pos, -radius * (15 / 40), radius * (10 / 40)), radius * (5 / 40))  # Left Eye
    Circle(modify_pos(center_pos, radius * (15 / 40), radius * (10 / 40)), radius * (5 / 40))  # Right Eye
    Line(modify_pos(center_pos, 0, radius * (10 / 40)), modify_pos(center_pos, -radius * (10 / 40), -radius * (10 / 40)), thickness=1)  # Nose Line 1
    Line(modify_pos(center_pos, -radius * (10 / 40), -radius * (10 / 40)),modify_pos(center_pos, radius * (10 / 40), -radius * (10 / 40)), thickness=1)  # Nose Line 2
    Arc(center_pos, radius * (30 / 40), 225, 90)  # Smile


# Specific Face Example
# Circle((300, 200), 40)
# Circle((285, 210), 5)
# Circle((315, 210), 5)
# Line((300, 210), (290, 190), thickness=1)
# Line((290, 190), (310, 190), thickness=1)
# Arc((300, 200), 30, 225, 90)


begin_graphics()

draw_face2(300, 200, scalar=2)
draw_face(500, 100)
draw_face2(100, 300, scalar=0.5)
draw_face(400, 250)

update_when("key_pressed")

end_graphics()
