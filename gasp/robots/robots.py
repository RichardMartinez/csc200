from gasp import *
from random import randint


class RobotsGame:
    UP_LEFT_KEY, UP_KEY, UP_RIGHT_KEY = "7", "8", "9"
    LEFT_KEY, TELEPORT_KEY, RIGHT_KEY = "4", "5", "6"
    DOWN_LEFT_KEY, DOWN_KEY, DOWN_RIGHT_KEY = "1", "2", "3"

    RIGHT_EDGE, TOP_EDGE = 63, 47
    LEFT_EDGE = BOTTOM_EDGE = 0

    def __init__(self):
        begin_graphics()
        self.finished = False
        self.player = Player()
        # self.robot = Robot()
        self.num_bots = 5

        self.robots = []
        for i in range(self.num_bots):
            robot = Robot()
            self.robots.append(robot)

        self.junk = []

    def next_move(self):
        self.player.move()

        surviving_robots = []
        for robot in self.robots:
            robot.move(self.player)

            other = robot.crashed(self.robots)
            if other or robot.crashed(self.junk):
                robot.shape = Box((10 * robot.x, 10 * robot.y), 10, 10, filled=True)
                self.junk.append(robot)
                if other in surviving_robots:
                    surviving_robots.remove(other)
            else:
                surviving_robots.append(robot)

        self.robots = surviving_robots

        if self.check_collisions(self.player, self.robots):
            self.finished = True
            Text("You lose!", (320, 240))
            sleep(3)

        if not self.robots:
            self.finished = True
            Text("You win!", (320, 240))
            sleep(3)

    @staticmethod
    def over():
        end_graphics()

    @staticmethod
    def check_collisions(thing, list_of_things):
        for item in list_of_things:
            if item.x == thing.x and item.y == thing.y:
                return True
        return False


class Player:
    def __init__(self):
        self.place()

    def place(self):
        self.x = randint(RobotsGame.LEFT_EDGE, RobotsGame. RIGHT_EDGE)
        self.y = randint(RobotsGame.BOTTOM_EDGE, RobotsGame.TOP_EDGE)
        self.shape = Circle((10 * self.x, 10 * self.y), 5, filled=True)

    def teleport(self):
        remove_from_screen(self.shape)
        self.place()

    def move(self):

        def up():
            if self.y < RobotsGame.TOP_EDGE:
                self.y += 1

        def down():
            if self.y > RobotsGame.BOTTOM_EDGE:
                self.y -= 1

        def left():
            if self.x > RobotsGame.LEFT_EDGE:
                self.x -= 1

        def right():
            if self.x < RobotsGame.RIGHT_EDGE:
                self.x += 1

        key = update_when("key_pressed")

        while key == RobotsGame.TELEPORT_KEY:
            self.teleport()
            key = update_when("key_pressed")

        if key == RobotsGame.UP_KEY:
            up()
        elif key == RobotsGame.DOWN_KEY:
            down()
        elif key == RobotsGame.LEFT_KEY:
            left()
        elif key == RobotsGame.RIGHT_KEY:
            right()
        elif key == RobotsGame.DOWN_LEFT_KEY:
            down()
            left()
        elif key == RobotsGame.DOWN_RIGHT_KEY:
            down()
            right()
        elif key == RobotsGame.UP_LEFT_KEY:
            up()
            left()
        elif key == RobotsGame.UP_RIGHT_KEY:
            up()
            right()

        move_to(self.shape, (10 * self.x, 10 * self.y))


class Robot:

    def __init__(self):
        self.place()

    def place(self):
        self.x = randint(RobotsGame.LEFT_EDGE, RobotsGame.RIGHT_EDGE)
        self.y = randint(RobotsGame.BOTTOM_EDGE, RobotsGame.TOP_EDGE)
        self.shape = Box((10 * self.x, 10 * self.y), 10, 10)

    def move(self, player):
        if self.x < player.x:
            self.x += 1
        elif self.x > player.x:
            self.x -= 1

        if self.y < player.y:
            self.y += 1
        elif self.y > player.y:
            self.y -= 1

        move_to(self.shape, (10 * self.x, 10 * self.y))

    def crashed(self, robots):
        for robot in robots:
            if self == robot:
                continue
            if self.x == robot.x and self.y == robot.y:
                return robot
        return False


game = RobotsGame()

while not game.finished:
    game.next_move()

game.over()