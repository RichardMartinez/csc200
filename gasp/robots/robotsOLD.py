from gasp import *
from random import randint


class Player:
    pass


class Robot:
    pass


begin_graphics()
finished = False
num_bots = 5


def place_player():
    global player

    player = Player()
    player.x = randint(0, 63)
    player.y = randint(0, 47)


def place_robots():
    global robots, num_bots

    robots = []

    while len(robots) < num_bots:
        robot = Robot()
        robot.x = randint(0, 63)
        robot.y = randint(0, 47)

        if not collided(robot, robots):
            robot.shape = Box((10*robot.x+5, 10*robot.y+5), 5, 5)
            robots.append(robot)


def move_player():
    global player

    def up():
        global player
        if player.y < 47:
            player.y += 1

    def down():
        global player
        if player.y > 0:
            player.y -= 1

    def left():
        global player
        if player.x > 0:
            player.x -= 1

    def right():
        global player
        if player.x < 63:
            player.x += 1

    key = update_when('key_pressed')

    while key == "5":
        remove_from_screen(player.shape)
        safely_place_player()
        key = update_when('key_pressed')

    if key == '8':
        up()
    elif key == '6':
        right()
    elif key == '2':
        down()
    elif key == '4':
        left()
    elif key == '9':
        right()
        up()
    elif key == '3':
        right()
        down()
    elif key == '1':
        left()
        down()
    elif key == '7':
        left()
        up()

    move_to(player.shape, (10*player.x+5, 10*player.y+5))


def move_robots():
    global robots, player

    def up(robot):
        if robot.y < 47:
            robot.y += 1

    def down(robot):
        if robot.y > 0:
            robot.y -= 1

    def left(robot):
        if robot.x > 0:
            robot.x -= 1

    def right(robot):
        if robot.x < 63:
            robot.x += 1

    for robot in robots:
        if robot.x < player.x:
            right(robot)
        elif robot.x > player.x:
            left(robot)

        if robot.y > player.y:
            down(robot)
        elif robot.y < player.y:
            up(robot)

        move_to(robot.shape, (10*robot.x+5, 10*robot.y+5))


def collided(thing, list_of_things):
    for item in list_of_things:
        if item.x == thing.x and item.y == thing.y:
            return True
    return False


def safely_place_player():
    global player, robots

    place_player()

    while collided(player, robots):
        place_player()

    player.shape = Circle((10 * player.x + 5, 10 * player.y + 5), 5, filled=True)


global robots, player

place_robots()
safely_place_player()

while not finished:
    move_player()
    move_robots()

    if collided(player, robots):
        finished = True
        Text("You've been caught!", (320, 240))
        sleep(3)

end_graphics()
