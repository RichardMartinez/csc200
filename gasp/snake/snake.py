from gasp import games
from gasp import boards
from gasp import color
from random import randint
from time import sleep

BOX_SIZE = 40
MARGIN = 60
SNAKE_COLOR = color.RED
EMPTY_COLOR = color.LIGHTGRAY


class SnakeBox(boards.GameCell):

    def __init__(self, board, i, j):
        self.init_gamecell(board, i, j)
        self.is_snake = 0
        self.is_food = 0
        self.food = None

    def snakify(self):
        self.is_snake = 1
        self.set_color(SNAKE_COLOR)

    def unsnakify(self):
        self.is_snake = 0
        self.set_color(EMPTY_COLOR)

    def foodify(self):
        self.is_food = 1
        self.food = Food(self.screen, self.screen_x, self.screen_y)

    def eat(self):
        self.is_food = 0
        self.food.destroy()
        self.board.new_food()


class SnakeBoard(boards.SingleBoard):

    def __init__(self, n_cols, n_rows, snake_size, interval):
        self.init_singleboard((MARGIN, MARGIN), n_cols, n_rows, BOX_SIZE)
        self.set_background_color(EMPTY_COLOR)
        self.game_over = 0

        self.create_directions(orthogonal_only=1, wrap=1)

        i = n_cols//2
        j = n_rows//2
        dir = boards.random_direction(orthogonal_only=1)
        self.snake = Snake(self, self.grid[i][j], dir)

        self.snake_size = snake_size
        self.interval = interval
        self.tick_count = 0

        snake_box = self.grid[i][j]
        dir = boards.turn_180(dir)
        for n in range(1, snake_size):
            snake_box = snake_box.direction[dir]
            self.snake.extend(snake_box)

        self.key_movements = {
            boards.K_UP: boards.UP,
            boards.K_DOWN: boards.DOWN,
            boards.K_LEFT: boards.LEFT,
            boards.K_RIGHT: boards.RIGHT
        }

        self.new_food()

    def keypress(self, key):
        if self.game_over:
            return

        try:
            direction = self.key_movements[key]
        except KeyError:
            return

        if direction == boards.turn_180(self.snake.direction):
            return

        self.snake.set_direction(direction)

    def new_gamecell(self, i, j):
        return SnakeBox(self, i, j)

    def tick(self):
        if self.game_over:
            score = f"You lost! Score: {len(self.snake.position) + 1}"
            games.Text(self.screen, 720//2, 720//2, score, size=35, color=color.BLUE)
            # self.screen.quit()  # Be done with main loop
            return

        self.tick_count += 1
        if self.tick_count == self.interval:
            self.tick_count = 0
            if not self.snake.move():
                self.game_over = 1

        self.limit_outlines(lambda box: box.is_snake, color.BLACK, EMPTY_COLOR)
        # self.draw_all_outlines()

    def new_food(self):
        while True:  # Keep going until found a safe spot
            i = randint(0, self._n_cols - 1)  # Pick coords
            j = randint(0, self._n_rows - 1)
            if not self.grid[i][j].is_snake:  # Is this space currently occupied?
                break  # No? Great!
        self.grid[i][j].foodify()  # Foodify that spot


class Snake:

    def __init__(self, board, box, direction):
        self.position = []
        self.position.append(box)
        self.direction = direction
        self.board = board

        box.snakify()

    def move(self):
        first = self.position[0].direction[self.direction]
        if first.is_food:
            first.eat()
        else:
            self.position.pop().unsnakify()

        if first.is_snake:
            return 0

        first.snakify()
        self.position.insert(0, first)
        return 1

    def extend(self, box):
        self.position.append(box)
        box.snakify()

    def set_direction(self, direction):
        self.direction = direction


class Food(games.Circle):

    def __init__(self, screen, x, y):
        self.init_circle(screen, x+BOX_SIZE//2, y+BOX_SIZE//2, BOX_SIZE//2 - 2, color.GREEN)


snakeGame = SnakeBoard(15, 15, 3, 10)
snakeGame.mainloop()
