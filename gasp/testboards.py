from gasp import games
from gasp import boards
from gasp import color
from gasp.utils import random_choice

BOX_SIZE = 30
WIDTH = 8
HEIGHT = 16
MARGIN = 2 * BOX_SIZE
EMPTY_COLOR = color.LIGHTGRAY


class myCell(boards.GameCell):
    def __init__(self, board, i, j):
        self.init_gamecell(board, i, j)
        # self.random = random_choice((0, 1))


class myBoard(boards.SingleBoard):
    def __init__(self):
        self.init_singleboard((MARGIN, MARGIN), WIDTH, HEIGHT, BOX_SIZE, cursor_color=color.SKYBLUE)
        self.set_background_color(EMPTY_COLOR)

        self.enable_cursor(1, 1)
        # self.disable_cursor()

        self.draw_all_outlines()
        # self.limit_outlines(lambda box: box.random, color.ORANGE, color.BLUE)

    def new_gamecell(self, i, j):
        return myCell(self, i, j)

    def keypress(self, key):
        self.handle_cursor(key)

        if key == games.K_RETURN:
            print(f"Selected grid space ({self.cursor.grid_x}, {self.cursor.grid_y})")
        elif key == games.K_BACKSPACE:
            self.move_cursor(0, 0)
        elif key == games.K_SPACE:
            print(self.on_board(8, 15))

    # def on_board(self, i, j):
    #     return (0 <= i < self._n_cols and 0 <= j < self._n_rows) and (not (2 <= i <= 5 and 3 <= j <= 12))

    # def cursor_moved(self):
    #     print("Moved")


screen = myBoard()
screen.mainloop()
