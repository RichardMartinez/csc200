from gasp import games
from gasp import boards
from gasp import color


class TestContain(boards.Container):
    def __init__(self, screen, x, y):
        self.init_container(['white_circle', 'red_circle'])

        self.white_circle = games.Circle(screen, 300, 300, 50, color.WHITE)
        self.red_circle = games.Circle(screen, 300, 300, 20, color.RED)

        # self.circle = games.Circle(screen, 300, 300, 50, color.RED)
        # self.text = games.Text(screen, 300, 300, "Tester", size=35, color=color.BLUE)
        # img = games.load_image("player.png")
        # img = games.scale_image(img, 4)
        # self.sprite = games.Sprite(screen, 300, 300, img)


class MyScreen(games.Screen):
    def keypress(self, key):
        if key == games.K_UP:
            circle.move_by(-100, -100)
        elif key == games.K_DOWN:
            circle.move_by(100, 100)

        if key == games.K_RIGHT:
            container.move_by(100, -100)
        elif key == games.K_LEFT:
            container.move_by(-100, 100)

        if key == games.K_SPACE:
            radius = container.red_circle.get_radius()
            container.red_circle.set_radius(radius + 5)
            # container.move_to(300, 300)
        elif key == games.K_RETURN:
            radius = container.red_circle.get_radius()
            container.red_circle.set_radius(radius - 5)
            # circle.move_to(300, 300)


        if key == games.K_BACKSPACE:
            print(container.overlapping_objects())


screen = MyScreen()
screen.set_background_color(color.LIGHTGRAY)

container = TestContain(screen)

circle = games.Circle(screen, 300, 300, 50, color.YELLOW)
circle.move_to(100, 100)

screen.mainloop()


# class MyScreen2(games.Screen):
#     def keypress(self, key):
#         if key == games.K_UP:
#             circle.move_by(100, 100)
#         elif key == games.K_DOWN:
#             circle.move_by(-100, -100)
#         elif key == games.K_RIGHT:
#             circle.move_to(300, 300)
#
#
# screen = MyScreen2()
# screen.set_background_color(color.LIGHTGRAY)
#
# circle = games.Circle(screen, 300, 300, 50, color=color.YELLOW)
#
# screen.mainloop()
