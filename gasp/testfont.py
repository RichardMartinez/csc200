from gasp import games
from gasp import color
from gasp.utils import random_choice

FONT_PATH = "8-bit-hud.ttf"
SYS_FONT = "CoRbEL"

FONT = FONT_PATH


class MyScreen(games.Screen):
    def __init__(self):
        self.init_screen()

        x, y = self.center_pos()
        self.text = games.Text(self, x, y, "Testing Text", size=30, color=color.RED, font=FONT)

    def mouse_down(self, pos, button):
        if button == 0:
            self.text.move_to(self.mouse_position())
        if button == 2:
            self.text.set_font(None)


screen = MyScreen()
screen.mainloop()
