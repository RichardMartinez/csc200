from gasp import games
from gasp import color


class TestRotate(games.Screen):
    def tick(self):
        global polygon, text, sprite, circle

        if self.is_pressed(games.K_LEFT):
            polygon.rotate_by(4)
        elif self.is_pressed(games.K_RIGHT):
            polygon.rotate_by(-4)

        if self.is_pressed(games.K_a):
            text.rotate_by(4)
        elif self.is_pressed(games.K_d):
            text.rotate_by(-4)

        if self.is_pressed(games.K_KP4):
            sprite.rotate_by(4)
        elif self.is_pressed(games.K_KP6):
            sprite.rotate_by(-4)

        if self.is_pressed(games.K_SPACE):
            self.set_title("Pressed Space!")

        pos = polygon.pos()
        circle.move_to(pos)


screen = TestRotate()
screen.set_title("Test Title Number One")

shape = [(0, 0), (6, -20), (12, 0)]
# shape = [(0, 0), (50, 0), (50, 50), (0, 50)]
polygon = games.Polygon(screen, 300, 300, shape, color.RED)

text = games.Text(screen, 100, 100, "Testing", size=35, color=color.RED)

IMG = games.load_image("player.png")
IMG = games.scale_image(IMG, 4)

sprite = games.Sprite(screen, 500, 400, IMG)

circle = games.Circle(screen, 300, 400, radius=10, color=color.YELLOW)

print(screen)
print(sprite)
print(text)
print(polygon)
print(circle)


screen.mainloop()
