from gasp import games
from gasp import color


class MyScreen(games.Screen):
    def tick(self):
        global line
        if self.is_pressed(games.K_RIGHT):
            line.rotate_by(-4)
        elif self.is_pressed(games.K_LEFT):
            line.rotate_by(4)


screen = MyScreen()
screen.set_background_color(color.LIGHTGRAY)

circle = games.Circle(screen, 300, 300, radius=50, color=color.YELLOW)

LENGTH = 100
THICKNESS = 5
vertical_line = [(0, 0), (0, LENGTH)]
horizontal_line = [(0, 0), (LENGTH, 0)]
diagonal_line = [(0, 0), (LENGTH, LENGTH)]

line = games.Line(screen, 300, 300, vertical_line, color.BLACK, thickness=3)
print(line.get_points())
line.set_points(horizontal_line)
print(line.get_points())

# for shape in [vertical_line, horizontal_line, diagonal_line]:
#     temp = games.Line(screen, 300, 300, shape, color.RED, thickness=THICKNESS)
#     temp.set_color(color.BLUE)

screen.mainloop()
