import json

with open("schedule.json") as f:
    schedule = json.load(f)

dash = "-"*40

print("Class Schedule for Richard Martinez 20-21")
print(dash)

for data in schedule:
    print(f"Period {schedule.index(data)+1}:")
    print(data["class"])
    print(data["time"])
    print(f"Day: {data['day']}")
    print(dash)
