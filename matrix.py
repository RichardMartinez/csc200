from fractions import Fraction


class Matrix:

    def __init__(self, list_of_lists):
        # m x n matrix
        self.storage = list_of_lists
        self.m = len(self.storage)
        self.n = len(self.storage[0])

    def __repr__(self):
        output = """"""
        for row in self.storage:
            output += self.fractionize_list(row)
        return output

    @staticmethod
    def fractionize_list(list):
        new_list = []
        for element in list:
            new_list.append(Matrix.fractionize_str(element))
        return f'|{" ".join(map(str, new_list))}|\n'

    @staticmethod
    def fractionize_str(string):
        element_str = str(string)
        if "." in element_str:
            fraction = Fraction(element_str).limit_denominator()
            if fraction.denominator != 1:
                element = f"{fraction.numerator}/{fraction.denominator}"
            else:
                element = str(int(string))
        else:
            element = str(int(string))

        return element

    def get_row(self, row_num):
        return self.storage[row_num - 1]

    def set_row(self, row_num, row_list):
        self.storage[row_num - 1] = row_list

    def get_column(self, col_num):
        column = []
        for index in range(self.m):
            column.append(self.get_row(index + 1)[col_num - 1])
        return column

    def element(self, i, j):
        return self.storage[i - 1][j - 1]

    def scale_row(self, row_num, scalar):
        new_storage = []
        for element in self.get_row(row_num):
            element *= scalar

            temp_str = str(element)
            if "." in temp_str:
                temp_list = temp_str.split(".")
                integer = temp_list[0]
                decimal = temp_list[1]

                if decimal == "0":
                    element = int(integer)

            new_storage.append(element)
        self.set_row(row_num, new_storage)

    def combine_row(self, row_num1, row_num2, scalar):
        # row1 + scalar * row2 -> row1
        new_storage = []
        row1 = self.get_row(row_num1)
        row2 = self.get_row(row_num2)
        for index in range(self.n):
            new_element = row1[index] + scalar * row2[index]

            if abs(new_element) <= 0.001:
                new_element = 0

            new_storage.append(new_element)
        self.set_row(row_num1, new_storage)

    def find_pivots(self):
        output = {}

        row_num = 1
        for row in self.storage:
            output[row_num] = None
            col_num = 1
            for element in row:
                if element != 0:
                    output[row_num] = col_num
                    continue
                col_num += 1
            row_num += 1

        return output

    def order_rows(self):
        pivots = self.find_pivots()
        ordered_lists = []

        for col_num in range(self.n):
            for row_num in range(self.m):
                if pivots[row_num + 1] == col_num + 1:
                    ordered_lists.append(self.get_row(row_num + 1))

        self.storage = Matrix(ordered_lists).storage

    def rref(self):
        if self.m > self.n:
            raise ValueError("Matricies with more rows than columns cannot be RREF'd.")

        self.order_rows()

        #####   GOING DOWN    ######

        for row_num in range(self.m):
            row_num += 1

            try:
                # print(row_num)
                scalar = 1 / self.element(row_num, row_num)
            except ZeroDivisionError:
                scalar = 0

            # print(f"{self.fractionize_str(scalar)} * R{row_num}")
            self.scale_row(row_num, scalar)

            for followup_row in range(row_num, self.m):
                followup_row += 1

                try:
                    scalar = -self.element(followup_row, row_num)/self.element(row_num, row_num)
                except ZeroDivisionError:
                    scalar = 0

                # print(f"R{followup_row} + {self.fractionize_str(scalar)} * R{row_num}")
                self.combine_row(followup_row, row_num, scalar)
                # print(self)

        #####    GOING UP    #####

        for row_num in reversed(range(1, self.m + 1)):

            for followup_row in reversed(range(1, row_num)):

                try:
                    scalar = -self.element(followup_row, row_num) / self.element(row_num, row_num)
                except ZeroDivisionError:
                    scalar = 0

                # print(f"R{followup_row} + {self.fractionize_str(scalar)} * R{row_num}")
                self.combine_row(followup_row, row_num, scalar)
                # print(self)


my_matrix = [
    [3, 2, 4, 2],
    [7, 3, 1, 3],
    [5, 7, 25, 5]
]

my_matrix2 = [
    [1, 2, -3, 1],
    [-3, -8, 7, 1]
]

my_matrix3 = [
    [1, 0, 1],
    [-2, 1, -3],
    [1, -2, 3],
    [0, 1, -1]
]

my_matrix4 = [
    [2, 8, 4, 2],
    [2, 5, 1, 5],
    [4, 10, -1, 1]
]

my_matrix5 = [
    [1, 2, 1],
    [-2, -3, 1],
    [3, 5, 0]
]

my_matrix6 = [
    [-3, -2, 3, 8, 7],
    [1, 1, -2, -2, 3],
    [-4, -3, 5, 10, 4],
    [2, 1, -1, -5, -5]
]

list_of_lists = my_matrix

final_matrix = Matrix(list_of_lists)
print(final_matrix)
final_matrix.rref()
print(final_matrix)
