CREATE TABLE "Members" ("name" TEXT, "email" TEXT, "phone_number" TEXT);

INSERT INTO Members (name, email, phone_number) VALUES ("John Swift", "john.swift12@gmail.com", "454-890-7248");
INSERT INTO Members (name, email, phone_number) VALUES ("Sally Sue", "sallys331@umich.edu", "152-343-2341");
INSERT INTO Members (name, email, phone_number) VALUES ("Mary Anne", "maryanne223@gmail.com", "545-435-3453");
INSERT INTO Members (name, email, phone_number) VALUES ("Joe Hamilton", "bigjmanhamilton@email.vccs.edu", "982-347-5029");
INSERT INTO Members (name, email, phone_number) VALUES ("Guy Dalton", "whosthatguy@gmail.com", "589-475-9348");
INSERT INTO Members (name, email, phone_number) VALUES ("Jack Small", "bestinthebiz@yahoo.com", "545-134-2406");

UPDATE Members SET name="Jonathan Hamilton" WHERE email="bigjmanhamilton@email.vccs.edu";

