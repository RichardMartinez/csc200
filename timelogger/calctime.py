from datetime import datetime
from datetime import timedelta

# Global Variables
DATE_FORMAT = "%m-%d-%Y %H:%M:%S"
total_seconds_worked = 0
dash = "-" * 30

# Read in the log file
filename = "SEtimesheet.log"
with open(filename, "r") as f:
    dataset = [line.strip().split() for line in f]

# {date_str: time_str}
days_worked = {}

# Loop through the data
for line in dataset:
    try:
        # Grab the next line too
        current_index = dataset.index(line)
        next_line = dataset[current_index + 1]
    except IndexError:
        # Reached the end of the file
        break

    # Grab current mode, date, and time
    mode = line[0]
    date, time = line[1], line[2]

    # Grab next mode, date, and time
    next_mode = next_line[0]
    next_date, next_time = next_line[1], next_line[2]

    # NEED TO HANDLE IMPROPER CLOCKING? (clock in but not out) (clock out but not in)
    # Clocked in and the back out
    if mode == "IN" and next_mode == "OUT":

        # Create datetime objects
        started = datetime.strptime(f"{date} {time}", DATE_FORMAT)
        finished = datetime.strptime(f"{next_date} {next_time}", DATE_FORMAT)

        # Find time worked
        time_worked = finished - started
        todays_date_str = started.strftime("%A, %B %d, %Y")

        # Assume second time is after the first (in case you go over midnight)
        if time_worked.days < 0:
            time_worked = timedelta(days=0,
                                    seconds=time_worked.seconds,
                                    microseconds=time_worked.microseconds)

        # Grab the number of seconds worked
        num_seconds = int(time_worked.total_seconds())

        # Update days_worked dictionary
        if todays_date_str not in days_worked.keys():  # First time clocking for the day
            days_worked[todays_date_str] = num_seconds  # Add todays date
        else:  # Already clocked in today
            days_worked[todays_date_str] += num_seconds  # Just add the seconds together

        # And install to global seconds worked
        total_seconds_worked += num_seconds


def convert_seconds(seconds):
    clock_time_worked = str(timedelta(seconds=seconds))

    lst = clock_time_worked.split(":")

    hours = lst[0]
    minutes = lst[1]
    seconds = lst[2]

    if "day" in hours:
        lst = hours.split()
        hours = int(lst[-1])
        days = int(lst[0])

        hours = hours + (days * 24)

    return hours, minutes, seconds


# Output
hours, minutes, seconds = convert_seconds(total_seconds_worked)

output = f"""Based on your log file {filename}, you have worked a total of:
{hours} hours, {minutes} minutes."""

print(output)

print(dash)

print("{:<30s} {:<30s}".format("DAY", "TIME WORKED"))

for date_str, seconds in days_worked.items():
    hours, minutes, seconds = convert_seconds(seconds)
    time_str = f"{hours} hours, {minutes} minutes"

    output = "{:<30s} {:<30s}".format(date_str, time_str)
    print(output)
