from sys import argv
import logging

"""
A short time logger that I plan to use to log my time working
during Senior Experience.
"""

# Example (optional message)
# python -m logtime in "start work with Caleb"
# python -m logtime out "finish work with Caleb"

# Read argv and how long it is
argv_len = len(argv)

# Ensure the correct number of arguments are passed
if argv_len not in [2, 3]:
    raise ValueError("Incorrect arguments. Try again.")

# Assign mode and msg
mode = argv[1].upper()

if mode == "I":
    mode = "IN"
elif mode == "O":
    mode = "OUT"

msg = ""
if argv_len == 3:  # Passed a msg
    msg = argv[2]

# Ensure valid mode
if mode not in ["IN", "OUT"]:
    raise ValueError("Invalid mode.")

# Define expected formats
LOG_FORMAT = f"{mode} %(asctime)s - %(message)s"
DATE_FORMAT = "%m-%d-%Y %H:%M:%S"

# Create logger object
filename = "SEtimesheet.log"
logging.basicConfig(filename=filename,
                    level=logging.DEBUG,
                    format=LOG_FORMAT,
                    datefmt=DATE_FORMAT)
logger = logging.getLogger()

# Log the result
logger.info(msg)
