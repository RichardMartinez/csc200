# I want to expand on scrapeUFD but this time put the data into an xml document instead of a raw text file.

# The reason I used lxml is because it allows for pretty print
from lxml import etree as ET

parser = ET.XMLParser(remove_blank_text=True)
tree = ET.parse("testdata.xml", parser)
root = tree.getroot()

print("-" * 10)
for country in root.iter("country"):
    name = country.get("name")
    gdppc = country.find("gdppc").text
    print(name, gdppc)
    for neighbor in country.iter("neighbor"):
        nname = neighbor.get("name")
        direction = neighbor.get("direction")
        print(nname, direction)
    print("-" * 10)

new_country = ET.SubElement(root, "newcountry")
new_country.set("name", "USA")
new_country.set("new", "yes")

testelem = ET.SubElement(new_country, "testelem")
testelem.set("new", "yes")
testelem.text = "Hello World!"

# tree.write("output/testoutput.xml", pretty_print=True)
