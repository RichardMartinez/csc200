from urllib.request import urlopen
from bs4 import BeautifulSoup as bSoup
import sys

# Get Char ID
charID = sys.argv[1]

# Url on Ultimate Frame Data for Joker
my_url = f"https://ultimateframedata.com/{charID}.php"

# Open URL, read HTML, close connection
try:
    client = urlopen(my_url)
except:
    sys.exit("That is not a valid URL.")

page_html = client.read()
client.close()

# HTML Parsing
page_soup = bSoup(page_html, "html.parser")

# Start Grabbing Data
char_name = page_soup.h1.text.strip().title()

# Grabs each move
move_containers = page_soup.find_all("div", {"class": "movecontainer"})
# move_containers[0] is description at the top of the page

# file = open("sephiroth.txt", "w", encoding="utf-8")

# Parse Data and Print
print(char_name)

# file.write(char_name + "\n")
for container in list(move_containers)[1:]:
    move_name = container.find("div", {"class": "movename"}).text.strip()

    try:
        move_startup = container.find("div", {"class": "startup"}).text.strip()
    except AttributeError:
        move_startup = ""

    try:
        move_advantage = container.find("div", {"class": "advantage"}).text.strip()

        if move_advantage in ["**", "--"]:
            move_advantage = ""
    except AttributeError:
        move_advantage = ""

    print(f"{move_name} | {move_startup} | {move_advantage} |")
    # file.write(f"{move_name} | {move_startup} | {move_advantage} |\n")

# file.close()
